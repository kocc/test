from math import log,factorial, sqrt
# n=1<<32
# print("\tlog(logn)\t=\t%f" % log(log(n,2),2))
# print("\tsq-logn\t=\t%f" % sqrt(log(n,2)))
# print("\tn^1/logn\t=\t%f" % n**(1/log(n,2)))
# print("logn**2\t=\t%f" % log(n,2)**2)
# print("2{sq-2logn}\t=\t%f" % 2**(sqrt(2*log(n,2))))
# print("\n\n")
# print("2^logn\t=\t%d" % 2**log(n,2))
# print("nlogn\t=\t", n*log(n,2))
# print("logn!\t=\t%d" % log(factorial(n),2))
# print("n2\t=\t%d" % (n*n))
# print("n!\t=\t%d" % factorial(n))
# print("2^2^n\t=\t%d" % 2**(2**n))
# print("n2^n\t=\t%d" % n*(2**n))



from asyncio import get_event_loop, coroutine, gather, async
from itertools import product
from pandas import Series, DataFrame, concat, Index


# @coroutine
# def align_and_call(aligner, dataset, **kwargs):
#     alignment = yield from aligner(dataset, **kwargs)
#     if alignment.is_completed:
#         benchmark = yield from vcbench(dataset, alignment.bam)
#         return Series([alignment, benchmark], index=Index(['alignment', 'benchmark'], name='workflow'))
#     return Series([alignment, None], index=Index(['alignment', 'benchmark'], name='workflow'))
#
#
# @coroutine
# def run_aligner(aligner, **kwargs):
#     results = yield from gather(*[align_and_call(aligner, ds, **kwargs) for ds in datasets])
#     return DataFrame(results, index=Index([ds.name for ds in datasets], name='dataset'))
#
#
# @coroutine
# def run_experiment():
#     results = yield from gather(*[run_aligner(aligner, threads=36) for aligner in aligners])
#     return concat(results,
#                   keys=[str(aligner.id) for aligner in aligners],
#                   names=['aligner'])
#
# loop = get_event_loop()
# results = loop.run_until_complete(run_experiment())


def generator(x):
  print(x)
  for i in x:
    yield i

async def direct(x):
  await x


if __name__ == '__main__':
    x = (('foo','off'), 'bar', 12)
    # x = 'foo'
    # loop = get_event_loop()
    # res = loop.run_until_complete(generator(x))
    l= list(generator(x))
    print(l)
    # list(direct(x))