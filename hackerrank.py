
def howManyCollisions(s):
    rcnt = 0
    dcnt = 0
    crash = 0
    for move in s:
        if move == "r":
            rcnt += 1
        elif move == "l":
            if rcnt:
                dcnt += 1
                crash += (1+rcnt)
                rcnt = 0

            elif dcnt:
                crash += 1
        else:
            if rcnt:
                dcnt += 1
                crash += rcnt
                rcnt = 0
            else:
                dcnt+=1
    return crash

def stringGameWinner(s, p):
    # Complete this function
    plen = len(p)
    slen = len(s)
    if plen > slen:
        return "Steven"

    if abs(plen-slen) % 2 != 0:
        ch = s[0]
        schek = s.replace(ch, "")
        pchek = p.replace(p[0], "")
        if pchek:
            return "Steven"
        elif not schek:
            if ch == p[0]:
                return "Amanda"
            else:
                return "Steven"

        p = p+p[0]

    plen = len(p)
    pbgn = int(slen/2 - plen/2)
    middle = s[pbgn:pbgn+plen]
    if middle == p:
        return "Amanda"
    else:
        return "Steven"


q = [
"abbbbba","bb",
"abb", "b"
]
i = 0
while i < len(q):
    # Returns the number of times moving robots collide.
    s = q[i]
    p = q[i+1]
    i+=2
    result = stringGameWinner(s,p)
    print(result)